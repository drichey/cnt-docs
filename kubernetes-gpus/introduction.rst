.. Date: June 16 2022
.. Author: drichey

.. _introduction:

############
Introduction
############

General overview of the section, in enough detail to help the reader understand whether to go further.

----

############
GPU Operator
############



----


Introduction
````````````
.. image:: graphics/nvidia-gpu-operator-image.jpg
   :width: 600

Kubernetes provides access to special hardware resources such as NVIDIA GPUs, NICs, Infiniband adapters and other devices
through the `device plugin framework <https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/device-plugins/>`_.
However, configuring and managing nodes with these hardware resources requires
configuration of multiple software components such as drivers, container runtimes or other libraries which are difficult
and prone to errors. The NVIDIA GPU Operator uses the `operator framework <https://coreos.com/blog/introducing-operator-framework>`_
within Kubernetes to automate the management of all NVIDIA software components needed to provision GPU. These components include the NVIDIA drivers (to enable CUDA),
Kubernetes device plugin for GPUs, the `NVIDIA Container Toolkit <https://github.com/NVIDIA/nvidia-docker>`_,
automatic node labelling using `GFD <https://github.com/NVIDIA/gpu-feature-discovery>`_, `DCGM <https://developer.nvidia.com/dcgm>`_ based monitoring and others.

----


Platform Support
````````````````

.. include:: gpu-operator/platform-support.rst

----

Release Notes
`````````````

.. include:: gpu-operator/release-notes.rst

----

Getting Started
```````````````
The :ref:`operator-install-guide` guide includes information on installing the GPU Operator in a Kubernetes cluster.

----

K8s (Upstream)
^^^^^^^^^^^^^^

VMWare Tanzu
^^^^^^^^^^^^

VMware Tanzu


OpenShift
^^^^^^^^^

Information about OpenShift.

Anthos
^^^^^^

Information about Anthos.

----

Advanced Configurations
```````````````````````

NVIDIA vGPU
^^^^^^^^^^^

GPU Direct RDMA
^^^^^^^^^^^^^^^

.. include:: gpu-operator/gpu-operator-rdma.rst

MIG
^^^

Air-gapped
^^^^^^^^^^
.. include:: gpu-operator/install-gpu-operator-air-gapped.rst


Fabric Manager
^^^^^^^^^^^^^^

Signed Driver
^^^^^^^^^^^^^

.. include:: gpu-operator/install-precompiled-signed-drivers.rst

KubeVirt
^^^^^^^^

GPU Sharing
^^^^^^^^^^^

Driver Manager
^^^^^^^^^^^^^^

vGPU Manager
^^^^^^^^^^^^

----

Troubleshooting
```````````````
.. include:: gpu-operator/troubleshootings.rst

----

Archive
```````

Previous GPU Operator Versions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

----

Device Plugin
=============

----

Introduction
````````````

----

License and Contributions
`````````````````````````
The NVIDIA Container Toolkit (and all included components) is licensed under `Apache 2.0 <https://www.apache.org/licenses/LICENSE-2.0>`_ and
contributions are accepted with a DCO. See the `contributing <https://github.com/NVIDIA/nvidia-container-toolkit/blob/master/CONTRIBUTING.md>`_ document for
more information.

----

Platform Support
````````````````

Platform support information.

----

Release Notes
`````````````

The release notes.

----

Getting Started
```````````````
----

Archive
```````
----

GPU Feature Discovery
=====================

----

Introduction
````````````

----

License and Contributions
`````````````````````````
The NVIDIA Container Toolkit (and all included components) is licensed under `Apache 2.0 <https://www.apache.org/licenses/LICENSE-2.0>`_ and
contributions are accepted with a DCO. See the `contributing <https://github.com/NVIDIA/nvidia-container-toolkit/blob/master/CONTRIBUTING.md>`_ document for
more information.

----

Platform Support
````````````````

Platform support information.

----

Release Notes
`````````````

The release notes.

----

Getting Started
```````````````

----

Archive
```````

----

.. include:: kubernetes-gpus/telemetry.rst

MIG with Kubernetes
===================

----

Introduction
````````````

----

License and Contributions
`````````````````````````
The NVIDIA Container Toolkit (and all included components) is licensed under `Apache 2.0 <https://www.apache.org/licenses/LICENSE-2.0>`_ and
contributions are accepted with a DCO. See the `contributing <https://github.com/NVIDIA/nvidia-container-toolkit/blob/master/CONTRIBUTING.md>`_ document for
more information.

----

Platform Support
````````````````

The :ref:`operator-platform-support` describes the supported platform configurations.

----

Release Notes
`````````````

Refer to :ref:`operator-release-notes` for information about releases.

----

Getting Started
```````````````

----

MIG Manager
```````````

This section concludes with information about the MIG Manager.

----

Introduction
^^^^^^^^^^^^

----

License and Contributions
^^^^^^^^^^^^^^^^^^^^^^^^^
The NVIDIA Container Toolkit (and all included components) is licensed under `Apache 2.0 <https://www.apache.org/licenses/LICENSE-2.0>`_ and
contributions are accepted with a DCO. See the `contributing <https://github.com/NVIDIA/nvidia-container-toolkit/blob/master/CONTRIBUTING.md>`_ document for
more information.

----

Platform Support
^^^^^^^^^^^^^^^^

Platform support information.

----

Release Notes
^^^^^^^^^^^^^

The release notes.

----

Getting Started
^^^^^^^^^^^^^^^

----

Archive
^^^^^^^

.. include:: archive.rst


